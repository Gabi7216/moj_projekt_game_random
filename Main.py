import FileUtility
import MainMenu

if __name__ == "__main__":
    FileUtility.createFile()
    FileUtility.readFromFile()
    menu = MainMenu.MainMenu()
    menu.startMainMenu()
