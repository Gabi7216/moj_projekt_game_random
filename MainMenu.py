import GameLogicMenu
import FileUtility

class MainMenu:

    main_menu_items = ["1 - Start Gry", "2 - Pokaz Ranking", "3 - EXIT"]

    def menu(self):
        print(f"{self.main_menu_items[0]}")
        print(f"{self.main_menu_items[1]}")
        print(f"{self.main_menu_items[2]}")

    def itemFromMenu(self):
        choose_item_from_main_menu = int(input("Choose item 1-3"))
        if choose_item_from_main_menu == 1:
            menu = GameLogicMenu.Menu()
            menu.start()
        elif choose_item_from_main_menu == 2:
           print(FileUtility.results)
            # dodac ze czeka na znak jakikolwiek i wraca do wyboru
            # od 1 do 3 z main_menu
        elif choose_item_from_main_menu == 3:
            FileUtility.writeToFile()
            return False
        return True

    def startMainMenu(self):
        start = True
        while start:
            self.menu()
            start = self.itemFromMenu()






