import random

class GameLogic:

    def getRandomNumber(self):
        self.random = random.randrange(0,10)

    def checkNumber(self, num):
        if num == self.random:
            return 0
        if num < self.random:
            return -1
        if num > self.random:
            return 1
        return 2